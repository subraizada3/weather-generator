# weather-gen
Generates HTML file for pages like the ones at [www.subraizada.com/weather](https://www.subraizada.com/weather/).

### Usage
`python weather-gen.py -lat 39.1406 -lon -86.6126 -wfo IND -tz EY5 -name bloomington_weather.html`

*Latitude and longitude must always have exactly 4 decimal places.*

See settings.txt for timezone (`-tz`) information.

A map of the WFOs is at wfo/wfo.svg.

You can customize which forecast panels (such as temperature, wind, air pressure, precipitation) are included in the generated page by editing the 'pcmd' configuration at the top of weather-gen.py. See settings.txt for information about what all the options do.

Optional arguments:
- `-unit`, `C` or `F`
- `-gset` and `-gdiff`, integer, see settings.txt
