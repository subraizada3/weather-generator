#!/usr/bin/env python3

# This downloads the weather plot for every bit in the PCMD bitstring
# Outputs: 6.png is the plot generated when the 6th bit in PCMD is set to 1 and
#          and rest are all set to 0
# Depends on the 'wget' library - pip install wget
# But it should not be hard to port this to the builtin requests library...

import wget
from time import sleep

#                                                          LAT         LON          WFO     ?????        ????    ?????   UNIT   ????     AHOUR   PCMD    LANG  ????        ??  BW  HRSPAN    ??????   ??????
url = 'https://forecast.weather.gov/meteograms/Plotter.php?lat=39.1406&lon=-86.6126&wfo=IND&zcode=INZ062&gset=18&gdiff=3&unit=0&tinfo=EY5&ahour=0&pcmd={}&lg=en&indu=1!1!1!&dd=&bw=&hrspan=48&pqpfhr=6&psnwhr=6'
# LAT/LON: must have 4 decimal places
# WFO: WFO that contains lat/lon
# AHOUR: hours to skip from current time to start forecast at, 0-48
# HRSPAN: hours of forecast, 0-48
# PCMD: see below
# LANG: en by default
# UNIT: 0 for F and inches, 1 for C and cm
# BW: 1 for black and white output

pstr = '00000000000000000000000000000000000000000000000000000000000'
partlen = len(pstr)

for i in range(partlen):
	# flip the ith bit from 0 to 1
	pstr2 = pstr[:i] + '1' + pstr[(i+1):]
	#print(url.format(pstr2)) # to see generated PCMD strings
	wget.download(url.format(pstr2), '{}.png'.format(i))
	sleep(1) # be nice to the NWS server
