#!/usr/bin/env python3

# This controls what is visible in the output chart. By default, all settings
#   up to and including 16/visibility are enabled. For a list of useful
#   settings, see settings.txt, or for a comprehensive list of settings, see
#   full-settings.txt
# The two #s below indicate settings  32 and 42
                                        #         #
pcmd = '11111111111111111000000000000000000000000000000000000000000'



import sys

args = {}

# collect data from command arguments
# this doesn't check that all arguments are present and correct

args['gset'] = '15' # default gust settings to 15 mph / 5 mph difference
args['gdiff'] = '5'
args['unit'] = '1' # default temperature unit to C

nameIndex = sys.argv.index('-name')
name = sys.argv[nameIndex + 1]
# if name is of the form "name.html", convert it to just "name"
if name[-5:] == '.html':
	name = name[:-5]

args['lat'] = sys.argv[sys.argv.index('-lat') + 1]
args['lon'] = sys.argv[sys.argv.index('-lon') + 1]
args['wfo'] = sys.argv[sys.argv.index('-wfo') + 1].upper()
args['tz'] = sys.argv[sys.argv.index('-tz') + 1].upper()
args['name'] = name
if '-unit' in sys.argv:
	args['unit'] = '0' if sys.argv[sys.argv.index('-unit') + 1].upper() == 'F' else '1'
if '-gdiff' in sys.argv:
	args['gdiff'] = sys.argv[sys.argv.index('-gdiff') + 1]
if '-gset' in sys.argv:
	args['gdiff'] = sys.argv[sys.argv.index('-gset') + 1]



# args has lat, lon, wfo, and name; generate
# first, define the template html
html = '''\
<html>
<head>
	<title>Weather</title>
	<style>
		.container { display: inline-block; }
		iframe { width: 850px; height: 960px; border: 0; }
	</style>
</head>

<body>
	<a href="https://forecast.weather.gov/MapClick.php?lat=$lat&lon=$lon">Standard forecast</a>

	<span>&nbsp;&nbsp;&nbsp;</span>

	<button onclick="switchUnits()">Switch units</button>

	<div style="white-space: nowrap">
		<div class="container">
			<img id="image1" src="http://forecast.weather.gov/meteograms/Plotter.php?lat=$lat&lon=$lon&wfo=$wfo&zcode=INZ062&gset=$gset&gdiff=$gdiff&unit=$unit&tinfo=$tz&ahour=0&pcmd=$pcmd&lg=en&indu=1!1!1!&dd=&bw=&hrspan=48&pqpfhr=6&psnwhr=6">
		</div>
		<div class="container">
			<img id="image2" src="http://forecast.weather.gov/meteograms/Plotter.php?lat=$lat&lon=$lon6&wfo=$wfo&zcode=INZ062&gset=$gset&gdiff=$gdiff&unit=$unit&tinfo=$tz&ahour=48&pcmd=$pcmd&lg=en&indu=1!1!1!&dd=&bw=&hrspan=48&pqpfhr=6&psnwhr=6">
		</div>
		<div class="container">
			<img id="image3" src="http://forecast.weather.gov/meteograms/Plotter.php?lat=$lat&lon=$lon&wfo=$wfo&zcode=INZ062&gset=$gset&gdiff=$gdiff&unit=$unit&tinfo=$tz&ahour=96&pcmd=$pcmd&lg=en&indu=1!1!1!&dd=&bw=&hrspan=48&pqpfhr=6&psnwhr=6">
		</div>
	</div>

	<script>
		function switchUnits() {
			const image1 = document.getElementById('image1');
			const image2 = document.getElementById('image2');
			const image3 = document.getElementById('image3');
			for (const image of [image1, image2, image3]) {
				if (image.src.indexOf("unit=1") != -1) { // unit is currently 1
					image.src = image.src.replace("unit=1", "unit=0");
				} else { // unit is currently 0
					image.src = image.src.replace("unit=0", "unit=1");
				}
			}
		}
	</script>
</body>
</html>
'''

# now, replace $lat, $lon, and $wfo in the html
html = html.replace('$lat', args['lat'])
html = html.replace('$lon', args['lon'])
html = html.replace('$wfo', args['wfo'])
html = html.replace('$unit', args['unit'])
html = html.replace('$gset', args['gset'])
html = html.replace('$gdiff', args['gdiff'])
html = html.replace('$tz', args['tz'])

html = html.replace('$pcmd', pcmd)

# write out html to output file
if args['name'].endswith('.html'):
	f = open(args['name'], 'w')
else:
	f = open(args['name'] + '.html', 'w')
f.write(html)
